# Deep Deterministic Policy Gradient

Projet realized as part of the course *Optimal Decision Making for Complex Problems* at the University of Liège.

This project is about using policy-based reinforcement learning methods to control a double inverted pendulum on a cart. We have chosen to implement the Deep Deterministic Policy Gradient algorithm (DDPG) with slight modifications.

This algorithm is compared with the Fitted Q-Iteration algorithm using Extra Trees as function approximator.

More information can be found in the [report](report.pdf) and in the [statement](statement.pdf).

## Contributors

- Yann Claes
- Gaspard Lambrechts
