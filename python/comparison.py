import matplotlib.pyplot as plt
import pybulletgym
import numpy as np
import time
import gym
import os

from sklearn.ensemble import ExtraTreesRegressor
from models import DDPG, FQI


if __name__ == '__main__':

    N_ACTIONS = 10
    T = 1000  # Maximum number of steps per episodes
    M = 300  # Number of episodes
    N = 30  # N for Q_N-functions
    S = 10  # Rate of training for the FQI (in terms of number of episodes)
    GAMMA = .95  # Discount factor

    # Setup
    gym.logger.set_level(40)

    # Environment
    env = gym.make('InvertedDoublePendulumPyBulletEnv-v0')
    x_dim = env.observation_space.shape[0]
    u_dim = env.action_space.shape[0]

    u_lower = env.action_space.low
    u_upper = env.action_space.high
    discrete_action_space = np.linspace(u_lower, u_upper, N_ACTIONS)

    # Agents
    ddpg = DDPG(x_dim=x_dim, action_space=env.action_space, h_dim=256,
                lr_actor=1e-4, lr_critic=1e-3, tau=1e-2, batch_size=32,
                buffer_size=2 ** 16, gamma=GAMMA, theta=.1, sigma=.1,
                n_actions=N_ACTIONS)
    model = ExtraTreesRegressor(n_estimators=50, n_jobs=-1)
    fqi = FQI(model, GAMMA, discrete_action_space, x_dim, u_dim, render=False,
              buffer=None)

    # Statistics
    rewards_ddpg = []
    rewards_fqi = []

    samples_ddpg = []
    samples_fqi = []

    time_ddpg = []
    time_fqi = []

    perf_ddpg = []
    perf_fqi = []

    total_time_ddpg = 0.
    total_time_fqi = 0.
    total_samples_ddpg = 0.

    for e in range(M):

        # Evaluate the current policy every S episodes
        if (e + 1) % S == 0:

            print('Episode {:03d} reached'.format(e + 1))

            # Evaluate DDPG
            perf = []
            for t in range(S):
                perf.append(ddpg.play(env, T))
            perf_ddpg.append(perf)

            # Add S epochs to fqi, evaluate on it, and then train on all
            perf = []
            for j in range(S):
                perf.append(fqi.play(env, T=T, store_transitions=True))
            perf_fqi.append(perf)

            start = time.time()
            fqi.train(N, verbose=False)
            elapsed = time.time() - start
            total_time_fqi += elapsed

            # Update statistics
            samples_ddpg.append(total_samples_ddpg)
            time_ddpg.append(total_time_ddpg)
            samples_fqi.append(len(fqi.buffer))
            time_fqi.append(total_time_fqi)

        # Train on one epoch
        start = time.time()
        _, steps = ddpg.train(env, T)
        elapsed = time.time() - start

        total_time_ddpg += elapsed
        total_samples_ddpg += steps

    env.close()

    # Performance wrt the number of episodes
    mean_perf_ddpg = np.mean(np.array(perf_ddpg), axis=1)
    std_perf_ddpg = np.std(np.array(perf_ddpg), axis=1)
    mean_perf_fqi = np.mean(np.array(perf_fqi), axis=1)
    std_perf_fqi = np.std(np.array(perf_fqi), axis=1)

    # Generate and save plots
    os.makedirs('plots', exist_ok=True)

    ep_range = range(S, len(mean_perf_ddpg) * S + 1, S)
    plt.plot(ep_range, mean_perf_ddpg, label='DDPG')
    plt.fill_between(ep_range, mean_perf_ddpg - std_perf_ddpg,
                     mean_perf_ddpg + std_perf_ddpg,
                     alpha=.3)
    plt.plot(ep_range, mean_perf_fqi, label='FQI')
    plt.fill_between(ep_range, mean_perf_fqi - std_perf_fqi,
                     mean_perf_fqi + std_perf_fqi,
                     alpha=.3)
    plt.xlabel('Number of episodes')
    plt.ylabel('Cumulative reward')
    plt.tight_layout()
    plt.legend()
    plt.savefig('plots/ddpg_vs_fqi_episodes.pdf')
    plt.show()

    # Performance wrt to the number of transitions
    plt.plot(samples_ddpg, mean_perf_ddpg, label='DDPG')
    plt.fill_between(samples_ddpg, mean_perf_ddpg - std_perf_ddpg,
                     mean_perf_ddpg + std_perf_ddpg,
                     alpha=.3)
    plt.plot(samples_fqi, mean_perf_fqi, label='FQI')
    plt.fill_between(samples_fqi, mean_perf_fqi - std_perf_fqi,
                     mean_perf_fqi + std_perf_fqi,
                     alpha=.3)
    plt.xlabel('Number of transitions')
    plt.ylabel('Cumulative reward')
    plt.tight_layout()
    plt.legend()
    plt.savefig('plots/ddpg_vs_fqi_transitions.pdf')
    plt.show()

    # Performance wrt to the time
    plt.plot(time_ddpg, mean_perf_ddpg, label='DDPG')
    plt.fill_between(time_ddpg, mean_perf_ddpg - std_perf_ddpg,
                     mean_perf_ddpg + std_perf_ddpg,
                     alpha=.3)
    plt.plot(time_fqi, mean_perf_fqi, label='FQI')
    plt.fill_between(time_fqi, mean_perf_fqi - std_perf_fqi,
                     mean_perf_fqi + std_perf_fqi,
                     alpha=.3)
    plt.xlabel('Training time [s]')
    plt.ylabel('Cumulative reward')
    plt.tight_layout()
    plt.legend()
    plt.savefig('plots/ddpg_vs_fqi_time.pdf')
    plt.show()
