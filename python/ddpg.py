import matplotlib.pyplot as plt
import pybulletgym
import numpy as np
import argparse
import time
import gym
import os

from models import DDPG


if __name__ == '__main__':

    # Parse parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--render', help='Render the game during '
                        'training', action='store_true')

    parser.add_argument('-M', '--n-episodes', help='Number of episodes',
                        type=int, default=400)
    parser.add_argument('-T', '--n-steps', help='Maximum number of transitions'
                        ' per episode', type=int, default=1000)
    parser.add_argument('-L', '--max-done', help='Allowed transitions in done '
                        'mode', type=int, default=5)

    parser.add_argument('-H', '--h-dim', help='Neurons in hidden layers',
                        type=int, default=256)
    parser.add_argument('-N', '--batch-size', help='Batch size',
                        type=int, default=32)
    parser.add_argument('-B', '--capacity', help='Replay buffer capacity',
                        type=int, default=2 ** 16)

    parser.add_argument('--gamma', help='Discount factor', type=float,
                        default=.95)

    parser.add_argument('--tau', help='Parameter for Poliak averaging',
                        type=float, default=1e-2)
    parser.add_argument('--lr-actor', help='Learning rate for the actor',
                        type=float, default=1e-4)
    parser.add_argument('--lr-critic', help='Learning rate for the critic',
                        type=float, default=1e-3)

    parser.add_argument('--n-actions', help='Whether to discretize each action'
                        ' dimension. Precise how many', type=int, default=None)
    args = parser.parse_args()

    # Setup
    gym.logger.set_level(40)

    # Environment
    env = gym.make('InvertedDoublePendulumPyBulletEnv-v0')
    x_dim = env.observation_space.shape[0]
    if args.render:
        env.render()

    # Agent
    ddpg = DDPG(x_dim=x_dim, action_space=env.action_space, h_dim=args.h_dim,
                lr_actor=args.lr_actor, lr_critic=args.lr_critic,
                tau=args.tau, batch_size=args.batch_size,
                buffer_size=args.capacity, gamma=args.gamma,
                render=args.render, theta=.1, sigma=.1,
                n_actions=args.n_actions)

    # Training loop
    rewards = []
    times_per_step = []
    policy_performances = []
    console_output = 'Episode {:04d}: {:3.2f} ({:.2f}s)'
    S = 10
    R = 20

    for e in range(args.n_episodes):

        # Evaluate the current policy every S episodes
        if (e + 1) % S == 0:
            policy_performance = []
            for t in range(R):
                policy_performance.append(ddpg.play(env, args.n_steps))
            policy_performances.append(policy_performance)

        start = time.time()

        # Train on one epoch and print the results of that epoch
        reward, steps = ddpg.train(env, args.n_steps, max_done=args.max_done)
        rewards.append(reward)

        elapsed = time.time() - start
        times_per_step.append(elapsed / steps)

        print(console_output.format(e + 1, reward, elapsed) +
              (' MAX' if steps == args.n_steps else ''))

    env.close()

    # Print statistics
    print('Mean time per step: {:.3f}s'.format(np.mean(times_per_step)))
    print('Mean scores online: {:.2f}'.format(np.mean(rewards)))

    # Plot training curve
    mean_perf = np.mean(np.array(policy_performances), axis=1)
    std_perf = np.std(np.array(policy_performances), axis=1)

    ep_range = range(S, len(mean_perf) * S + 1, S)
    plt.plot(ep_range, mean_perf)
    plt.fill_between(ep_range, mean_perf - std_perf, mean_perf + std_perf,
                     alpha=.3)

    filename = 'DDPG-T{}-L{}-N{}-B{}-H{}-{:.2f}'
    filename = filename.format(args.n_steps,args.max_done, args.batch_size,
                               args.capacity, args.h_dim, np.mean(rewards))
    # Save training curve
    os.makedirs('plots', exist_ok=True)
    plt.xlabel('Number of episodes')
    plt.ylabel('Cumulative reward')
    plt.tight_layout()
    plt.savefig('plots/{}.pdf'.format(filename))
    plt.show()
