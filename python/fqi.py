import matplotlib.pyplot as plt
import pybulletgym
import numpy as np
import argparse
import time
import gym
import os

from sklearn.ensemble import ExtraTreesRegressor
from models import FQI


if __name__ == '__main__':

    # Setup
    gym.logger.set_level(40)

    # Environment
    env = gym.make('InvertedDoublePendulumPyBulletEnv-v0')
    x_dim = env.observation_space.shape[0]
    u_dim = env.action_space.shape[0]

    n_actions = 10
    u_lower = env.action_space.low
    u_upper = env.action_space.high
    discrete_action_space = np.linspace(u_lower, u_upper, n_actions)

    gamma = 0.95

    model = ExtraTreesRegressor(n_estimators=50, n_jobs=-1)
    fqi = FQI(model, gamma, discrete_action_space, x_dim, u_dim, render=False,
              buffer=None)

    M = 200  # Number of episodes
    T = 300  # Maximum time steps per episode
    S = 10  # Number of episodes between two performance estimations
    N = 30  # N of the Q_N-fuctions

    fqi.fill_buffer(env, nb=(T * S), T=T)

    rewards = []
    for i in range(0, M, S):

        print('Episode {:03d}: the buffer now contains {} transitions'
              .format(i, len(fqi.buffer)))

        start = time.time()

        # Train on all previous epochs
        fqi.train(N, verbose=False)

        elapsed = time.time() - start
        print('{} epochs trained in {:.2f}s'.format(S, elapsed))

        # Estimate
        reward = []
        for j in range(S):
            reward.append(fqi.play(env, T=T, store_transitions=True))
        rewards.append(reward)

        elapsed = time.time() - start - elapsed
        print('Evaluated on {} run in {:.2f}s'.format(S, elapsed))

    # Generate training curve
    mean_perfs = np.mean(rewards, axis=1)
    std_perfs = np.std(rewards, axis=1)
    ep_range = range(S, M + 1, S)
    plt.plot(ep_range, mean_perfs)
    plt.fill_between(ep_range, mean_perfs - std_perfs,
                     mean_perfs + std_perfs, alpha=.4)

    # Save training curve
    os.makedirs('plots', exist_ok=True)
    plt.xlabel('Number of episodes')
    plt.ylabel('Cumulative reward')
    plt.tight_layout()
    plt.savefig('plots/FQI.pdf')
    plt.show()
