import numpy as np
import torch
import torch.nn as nn

from torch.optim import Adam
from torch.nn.functional import mse_loss
from sklearn.neighbors import NearestNeighbors

from utils import ReplayBuffer, OrnsteinUhlenbeckAction


class Critic(nn.Module):
    """
    Critic is the implementation of the neural network architecture used below.
    It is characterized by two hidden layers of equal size, using ReLU
    functions as activation functions, and with no final activation function.
    """

    def __init__(self, input_dim, hidden_dim, out_dim):
        """
        Parameters
        ----------
        input_dim: integer
            The dimension of the input layer
        hidden_dim: integer
            The dimension of both hidden layers
        out_dim: integer
            The dimension of the output layer
        """
        super(Critic, self).__init__()

        self.sequential = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.LayerNorm(hidden_dim),
            nn.ReLU(inplace=True),

            nn.Linear(hidden_dim, hidden_dim),
            nn.LayerNorm(hidden_dim),
            nn.ReLU(inplace=True),

            nn.Linear(hidden_dim, out_dim),
        )

    def forward(self, x):
        """
        Compute the forward pass for the input. The latter should have the
        appropriate dimensions.

        Parameters
        ----------
        x: tensor of shape [input_dim]
            The input tensor

        Return
        ------
        x: tensor of shape [out_dim]
            The output tensor
        """

        return self.sequential(x)


class Actor(nn.Module):
    """
    Actor is the implementation of the neural network architecture used below.
    It is characterized by two hidden layers of equal size, using ReLU
    functions as activation functions, and a tanh as final activation.

    NB: the tanh is very tailored to our problem and could be a problem in
    environments with continuous actions that range outside of the [-1, 1]
    interval.
    """

    def __init__(self, input_dim, hidden_dim, out_dim):
        """
        Parameters
        ----------
        input_dim: integer
            The dimension of the input layer
        hidden_dim: integer
            The dimension of both hidden layers
        out_dim: integer
            The dimension of the output layer
        """
        super(Actor, self).__init__()

        self.sequential = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.LayerNorm(hidden_dim),
            nn.ReLU(inplace=True),

            nn.Linear(hidden_dim, hidden_dim),
            nn.LayerNorm(hidden_dim),
            nn.ReLU(inplace=True),

            nn.Linear(hidden_dim, out_dim),
            nn.Tanh(),
        )

    def forward(self, x):
        """
        Compute the forward pass for the input. The latter should have the
        appropriate dimensions.

        Parameters
        ----------
        x: tensor of shape [input_dim]
            The input tensor

        Return
        ------
        x: tensor of shape [out_dim]
            The output tensor
        """

        return self.sequential(x)


class DDPG():
    """
    DDPG is the implementation of our cartpole agent. It is characterized by
    several specific functions along with some more general functions to train
    the agent, play an action/episode, etc. It is based on the Deep
    Deterministic Policy Gradient algorithm.
    """

    def __init__(self, x_dim, action_space, h_dim, lr_actor, lr_critic, tau,
                 batch_size, buffer_size, gamma, theta, sigma, render=False,
                 n_actions=None):
        """
        Parameters
        ----------
        x_dim: integer
            The state space dimension
        action_space: ndarray of shape [action_dim]
            The environment action space
        h_dim: integer
            The size of the hidden layers for the agent's networks
        lr_actor: float
            The learning rate for the agent's actor network
        lr_critic: float
            The learning rate for the agent's critic network
        tau: float
            The averaging parameter for poliak averaging
        batch_size: integer
            The batch size to consider
        gamma: float
            The discount factor
        render: boolean
            Whether or not to render the environment when training/playing
        theta: float
            The first parameter for the agent's Ornstein-Uhlenbeck process
        sigma: float
            The second parameter for the agent's Ornstein-Uhlenbeck process
        n_actions: integer or None
            None if considering continuous actions, or the number of discrete
            action to consider
        """
        self.action_space = action_space

        self.u_dim = action_space.shape[0]
        self.x_dim = x_dim
        self.xu_dim = x_dim + self.u_dim
        self.h_dim = h_dim

        self.lr_actor = lr_actor
        self.lr_critic = lr_critic
        self.tau = tau
        self.gamma = gamma
        self.render = render
        self.n_actions = n_actions

        if self.n_actions is not None:
            # Creation of an equally spaced mesh on the action state
            dim_linspace = np.linspace(start=self.action_space.low,
                                       stop=self.action_space.high,
                                       num=n_actions)
            grid_space = torch.Tensor(np.meshgrid(*dim_linspace.T))

            # Initialize the nearest neighbors if discrete environment
            self.disc_action_space = grid_space.reshape(self.u_dim, -1).T
            self.nn = NearestNeighbors(n_neighbors=self.n_actions // 2)
            self.nn.fit(self.disc_action_space)

        # Initialize the replay buffer
        self.batch_size = batch_size
        transition_dim = x_dim + self.u_dim + 1 + x_dim + 1  # x, u, r, y, d
        self.buffer = ReplayBuffer(capacity=buffer_size,
                                   transition_dim=transition_dim)

        # Initialize the noise process
        self.noise = OrnsteinUhlenbeckAction(self.action_space, theta=theta,
                                             sigma=sigma)

        # Actor and critic network + target networks
        self.actor = Actor(x_dim, h_dim, self.u_dim)
        self.actor_tar = Actor(x_dim, h_dim, self.u_dim)
        self.critic = Critic(x_dim + self.u_dim, h_dim, 1)
        self.critic_tar = Critic(x_dim + self.u_dim, h_dim, 1)

        # Optimizers
        self.optim_critic = Adam(self.critic.parameters(), lr=lr_critic)
        self.optim_actor = Adam(self.actor.parameters(), lr=lr_actor)

        # Initializing the targets weights equal to the estimators weights
        self.critic_tar.load_state_dict(self.critic.state_dict())
        self.actor_tar.load_state_dict(self.actor.state_dict())

        # Targets networks are only used in forward pass
        self.critic_tar.eval()
        self.actor_tar.eval()

    def poliak_averaging(self):
        """
        Update the agent's actor and critic target networks using poliak
        averaging.
        """
        for wct, wc in zip(self.critic_tar.parameters(),
                           self.critic.parameters()):
            wct.data.copy_(self.tau * wc + (1.0 - self.tau) * wct)

        for wat, wa in zip(self.actor_tar.parameters(),
                           self.actor.parameters()):
            wat.data.copy_(self.tau * wa + (1.0 - self.tau) * wat)

    def wolpertinger_policy(self, s, noise=False):
        """
        Compute a discrete action from a given state.
        https://arxiv.org/pdf/1512.07679.pdf

        Parameters
        ----------
        s: tensor of shape [n_state, x_dim]
            The states from which to compute the discrete actions.
        noise: boolean
            Whether or not the initial continuous action should be made noisy
            using the Ornstein-Uhlenbeck process

        Return
        ------
        best_action: tensor of shape [n_state, u_dim]
            The computed discrete action (one for each state)
        """
        u = self.noise(self.actor_tar(s)) if noise else self.actor_tar(s)

        k = self.nn.n_neighbors

        # Compute the k nearest actions for each state
        nn_idx = self.nn.kneighbors(u, return_distance=False)
        nn_actions = self.disc_action_space[nn_idx.ravel()]
        nn_actions = nn_actions.view((-1, k, self.u_dim))

        # Compute the performance of all considered state-action pairs
        U = nn_actions.view((-1, self.u_dim))
        XU = torch.cat((s.repeat_interleave(k, dim=0), U), dim=1)

        # Choose the best action accordingly
        best_choices = self.critic(XU).reshape((-1, k)).argmax(axis=1)

        return nn_actions[range(len(nn_actions)), best_choices]

    def action(self, env, x=None, noise=False):
        """
        Compute an action from a given state and perform the corresponding
        transition.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to perform the action
        x: tensor of shape [x_dim]
            The state from which to compute the action
        noise: boolean
            Whether or not the action should be made noisy using the
            Ornstein-Uhlenbeck process

        Return
        ------
        x: tensor of shape [x_dim]
            The initial state of the transition
        u: tensor of shape [u_dim]
            The action performed in the transition
        r: tensor of shape [1]
            The reward obtained in the transition
        y: tensor of shape [x_dim]
            The state obtained by playing action u in state x
        d: boolean
            Whether or not the transition lead to a terminal state
        """
        if x is None:
            x = torch.tensor(env.reset()).float()

        # Remember the network mode
        train = self.actor.training
        if train:
            self.actor.train()

        # Choose action
        with torch.no_grad():
            if self.n_actions is not None:
                u = self.wolpertinger_policy(x.unsqueeze(0), noise).squeeze(0)
            else:
                u = self.actor(x.unsqueeze(0)).squeeze(0)
                if noise:
                    u = self.noise(u)

        # Restore network mode
        self.actor.train(mode=train)

        # Take action and store results
        y, r, d, _ = env.step(u)
        y = torch.tensor(y).float()
        r = torch.tensor([r]).float()
        d = torch.tensor([d]).float()

        return x, u, r, y, d

    def get_views(self, transitions, return_xu=False):
        """
        Provide useful views for training from a set of transitions.

        Parameters
        ----------
        transitions: tensor of shape [batch_size, 2 * x_dim + u_dim + 2]
            The batch of transitions from which to return the views
        return_xu: boolean
            Whether or not to return a view composed of both the initial states
            and the actions performed in the batch of transitions

        Return
        ------
        X: tensor of shape [batch_size, x_dim]
            The view of all initial states in the transitions
        U: tensor of shape [batch_size, u_dim]
            The view of all actions taken in the transitions
        XU: tensor of shape [batch_size, x_dim + u_dim]
            The view of all initial states and actions (combined) in the
            transitions
        R: tensor of shape [batch_size, 1]
            The view of all rewards obtained in the transitions
        Y: tensor of shape [batch_size, x_dim]
            The view of all next states obtained in the transitions
        D: tensor of shape [batch_size, 1]
            The view of all booleans indicating whether or not the agent
            reached a terminal state each transition
        """
        X = transitions[:, :self.x_dim]
        U = transitions[:, self.x_dim:self.xu_dim]
        XU = transitions[:, :self.xu_dim]
        R = transitions[:, self.xu_dim:self.xu_dim + 1]
        Y = transitions[:, - self.x_dim - 1:-1]
        D = transitions[:, -1:]

        if return_xu:
            return X, U, R, Y, D, XU
        return X, U, R, Y, D

    def get_target(self, R, Y, D):
        """
        Compute the targets for the agent's loss function, using its target
        networks. Targets correspond to an estimate of the Q-function using the
        one-step bootstrapping.

        Parameters
        ----------
        R: tensor of shape [batch_size, 1]
            The rewards with which to compute the targets
        Y: tensor of shape [batch_size, x_dim]
            The next states with which to compute the targets
        D: tensor of shape [batch_size, 1]
            The booleans indicating whether or not the corresponding transition
            lead to a terminal state

        Return
        ------
        targets: tensor of shape [batch_size, 1]
            The computed targets
        """
        with torch.no_grad():
            if self.n_actions is not None:
                next_U = self.wolpertinger_policy(Y)
            else:
                next_U = self.actor_tar(Y)

            next_Q = self.critic_tar(torch.cat((Y, next_U), axis=1))
            targets = R + (1 - D) * self.gamma * next_Q

        return targets

    def play(self, env, T):
        """
        Play an episode of a given length with the agent's current networks. If
        the episode reaches a terminal state before its predefined length, the
        episode is ended.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to play the episode
        T: integer
            The length of the episode

        Return
        ------
        reward: float
            The reward obtained during the episode
        """
        self.actor.eval()
        self.actor.eval()

        reward = 0
        y = torch.tensor(env.reset()).float()

        for t in range(T):

            # Next state becomes the current state
            x = y
            x, u, r, y, d = self.action(env, x, noise=False)
            if self.render:
                env.render()

            # Reached terminal state, abort episode
            if d:
                break

            # Update cumulative reward
            reward += (1 - d) * (self.gamma ** t) * r

        return float(reward)

    def train(self, env, T, max_done=0):
        """
        Train the agent's networks with an episode of a given maximum length.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to train the agent's networks
        T: integer
            The maximum length of the training episode
        max_done: integer
            The number of allowed transitions after reaching a terminal state

        Return
        ------
        reward: float
            The reward obtained during the training episode
        t: integer
            The true length of the episode
        """
        self.actor.train()
        self.critic.train()

        reward = 0.
        time_done = 0
        y = torch.tensor(env.reset()).float()

        for t in range(T):

            # Next state becomes the current state
            x = y

            # Execute action and observe reward
            x, u, r, y, d = self.action(env, x, noise=True)
            if self.render:
                pass  # env.render()

            # Update cumulative reward and number of steps in terminal states
            reward += (1 - d) * (self.gamma ** t) * r
            time_done += d

            # Store transition in the replay buffer
            self.buffer.add(torch.cat((x, u, r, y, d), axis=0))

            # Sample a random minibatch of N transitions and deduce targets
            transitions = self.buffer.sample(self.batch_size)
            X, U, R, Y, D, XU = self.get_views(transitions, return_xu=True)
            targets = self.get_target(R, Y, D)

            # Update the critic: towards the target (MSE)
            self.optim_critic.zero_grad()
            critic_output = self.critic(XU)
            critic_loss = mse_loss(critic_output, targets)
            critic_loss.backward()
            self.optim_critic.step()

            # Update the actor: using the sampled policy gradient
            self.optim_actor.zero_grad()
            actor_loss = - \
                self.critic(torch.cat((X, self.actor(X)), axis=1)).mean()
            actor_loss.backward()
            self.optim_actor.step()

            self.poliak_averaging()

            if time_done > max_done:
                break

        return float(reward), t + 1


class FQI():
    """
    FQI is the implementation of our online FQI agent. It is characterized by
    several specific functions along with some more general functions to train
    the agent, append new transitions to the buffer of transitions, etc.
    """

    def __init__(self, model, gamma, actions, x_dim, u_dim, render=False,
                 buffer=None):
        """
        Parameters
        ----------
        model: sklearn estimator
            The model used as approximator, should have `fit` and `predict`
            methods
        gamma: float
            The discount factor
        actions: array
            The list of discrete actions to consider (e.g. linspace)
        x_dim: integer
            The state space dimension
        u_dim: integer
            The action space dimension
        render: bool
            Whether to render the game after each action taken
        buffer: list or None
            An eventual buffer of transitions on which to train
        """
        self.model = model
        self.predict = lambda XU: np.random.random(len(XU))
        self.gamma = gamma
        self.actions = np.array(actions)

        self.x_dim = x_dim
        self.u_dim = u_dim
        self.xu_dim = x_dim + u_dim

        self.render = render

        if buffer is None:
            self.buffer = []
        else:
            self.buffer = buffer

    def get_views(self, transitions, return_xu=False):
        """
        Provide useful views for training from a set of transitions.

        Parameters
        ----------
        transitions: tensor of shape [batch_size, 2 * x_dim + u_dim + 2]
            The batch of transitions from which to return the views
        return_xu: boolean
            Whether or not to return a view composed of both the initial states
            and the actions performed in the batch of transitions

        Return
        ------
        X: tensor of shape [batch_size, x_dim]
            The view of all initial states in the transitions
        U: tensor of shape [batch_size, u_dim]
            The view of all actions taken in the transitions
        XU: tensor of shape [batch_size, x_dim + u_dim]
            The view of all initial states and actions (combined) in the
            transitions
        R: tensor of shape [batch_size, 1]
            The view of all rewards obtained in the transitions
        Y: tensor of shape [batch_size, x_dim]
            The view of all next states obtained in the transitions
        D: tensor of shape [batch_size, 1]
            The view of all booleans indicating whether or not the agent
            reached a terminal state each transition
        """
        X = transitions[:, :self.x_dim]
        U = transitions[:, self.x_dim:self.xu_dim]
        XU = transitions[:, :self.xu_dim]
        R = transitions[:, self.xu_dim:self.xu_dim + 1]
        Y = transitions[:, - self.x_dim - 1:-1]
        D = transitions[:, -1:]

        if return_xu:
            return X, U, R, Y, D, XU
        return X, U, R, Y, D

    def action(self, env, x=None):
        """
        Compute an action from a given state and perform the corresponding
        transition on the environment.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to perform the action
        x: array of shape [x_dim]
            The state from which to compute the action

        Return
        ------
        x: array of shape [x_dim]
            The initial state of the transition
        u: array of shape [u_dim]
            The action performed in the transition
        r: float
            The reward obtained in the transition
        y: array of shape [x_dim]
            The state obtained by playing action u in state x
        d: boolean
            Whether or not the transition lead to a terminal state
        """
        if x is None:
            x = env.reset()

        # Choose best action
        x_repeated = np.tile(x, (len(self.actions), 1))
        u_repeated = self.actions.reshape(-1, self.u_dim)
        XU = np.concatenate((x_repeated, u_repeated), axis=1)
        Q = self.predict(XU).reshape(-1, len(self.actions))
        u = self.actions[np.argmax(Q, axis=1)].reshape(1)

        # Take action and store results
        y, r, d, _ = env.step(u)

        return x, u, r, y, d

    def fill_buffer(self, env, nb, T, max_done=0):
        """
        Fill the buffer of transitions with `nb` new transitions. Each
        trajectory is of length `T` at most.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to perform the action
        nb: array of shape [x_dim]
            The number of transitions to add to the buffer
        T: integer
            The maximum length of the episodes that generate these transitions
        max_done: integer
            The number of allowed transitions after reaching a terminal state
        """
        if self.buffer is None:
            self.buffer = []

        done = 0
        for i in range(nb):

            if not i % T:
                x = env.reset()

            x, u, r, y, d = self.action(env, x)
            self.buffer.append(np.concatenate([x, u, [r], y, [d]], axis=0))

            done += d
            if done > max_done:
                x = env.reset()

    def train(self, N=30, verbose=False):
        """
        Fit the agent's estimator on the current buffer using N fitted Q
        iterations.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to train the agent's estimator
        N: integer
            The number of fitted Q iterations to perform
        verbose: bool
            Whether to print the iteration numbers
        """
        if not self.buffer:
            raise ValueError('Found empty buffer of transitions')

        transitions = np.array(self.buffer)
        X, U, R, Y, D, XU = self.get_views(transitions, return_xu=True)

        repeated_Y = np.repeat(Y, len(self.actions), axis=0)
        repeated_V = np.tile(self.actions, (len(Y), 1))
        YV = np.concatenate((repeated_Y, repeated_V), axis=1)

        self.predict = lambda XU: np.zeros((len(XU),))

        for i in range(N):

            if verbose:
                print('Iteration {:02d}'.format(i + 1))

            Q_next = self.predict(YV).reshape(
                (-1, len(self.actions))).max(axis=1)
            targets = R.ravel() + self.gamma * (1 - D.ravel()) * Q_next

            self.model.fit(XU, targets)
            self.predict = self.model.predict

    def play(self, env, T, store_transitions=False):
        """
        Play an episode of a given length with the agent's current fitted
        estimator. If the episode reaches a terminal state before its
        predefined length, the episode is ended.

        Parameters
        ----------
        env: gym/pybullet environment
            The environment in which to play the episode
        T: integer
            The length of the episode
        store_transitions: bool
            Whether to store the generated transitions in the buffer of
            transitions

        Return
        ------
        reward: float
            The reward obtained during the episode
        """
        reward = 0
        y = torch.tensor(env.reset()).float()

        for t in range(T):

            # Next state becomes the current state
            x = y
            x, u, r, y, d = self.action(env, x)

            if store_transitions:
                self.buffer.append(np.concatenate((x, u, [r], y, [d])))

            # Reached terminal state, abort episode
            if d:
                break

            # Update cumulative reward
            reward += (1 - d) * (self.gamma ** t) * r

        return reward
