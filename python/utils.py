import numpy as np
import torch

from torch.distributions.multivariate_normal import MultivariateNormal


class OrnsteinUhlenbeckAction():
    """
    OrnsteinUhlenbeckAction is the implementation of a class sampling noisy
    actions according to an Ornstein-Uhlenbeck process, where the noise is
    drawn from a (multivariate) normal distribution.
    """

    def __init__(self, action_space, theta, sigma, reset_interval=50, mu=0.0):
        """
        Parameters
        ----------
        action_space: ndarray of shape [action_dim]
            The environment action space
        theta: float
            The first parameter of the Ornstein-Uhlenbeck process
        sigma: float
            The second parameter of the Ornstein-Uhlenbeck process
        reset_interval: integer
            The number of steps after which to reset the noisy process
        mu: float
            The third parameter of the Ornstein-Uhlenbeck process
        """
        self.theta = theta
        self.sigma = sigma
        self.mu = mu

        self.dim = action_space.shape[0]

        # Random normal noise
        self.normal = MultivariateNormal(torch.zeros(self.dim),
                                         torch.eye(self.dim))

        # Action bounds
        self.lower = action_space.low[0]
        self.upper = action_space.high[0]

        # Process state
        self.reset_noise()
        self.t = 0
        self.reset_interval = reset_interval

    def reset_noise(self):
        """
        Reset the noise process.
        """

        self.x = self.normal.sample() * (self.upper - self.lower) / 16

    def __call__(self, action):
        """
        Compute a noisy action from an input action.

        Parameters
        ----------
        action: tensor of shape [dim]
            The initial action

        Return
        ------
        noisy_action: tensor of shape [dim]
            The noisy action resulting from applying the process to the initial
            action
        """
        if not self.t % self.reset_interval:
            self.reset_noise()

        # Compute next noise value
        dx = (self.theta * (self.mu - self.x) +
              self.sigma * self.normal.sample())
        self.x += dx
        self.t += 1

        return torch.clamp(action + self.x, self.lower, self.upper)


class ReplayBuffer():
    """
    ReplayBuffer is the implementation of a class representing a traditional
    replay buffer, from which one can sample a batch of transitions.
    """

    def __init__(self, capacity, transition_dim):
        """
        Parameters
        ----------
        capacity: integer
            The maximum capacity of the replay buffer
        transition_dim: integer
            The dimension of a transition
        """
        self.capacity = capacity
        self.transition_dim = transition_dim
        self.buffer = torch.empty(capacity, transition_dim)
        self.nb = 0
        self.older = 0

    def add(self, transition):
        """
        Add a transition to the replay buffer. If the replay buffer is full, it
        replaces the oldest transition with the new transition.

        Parameters
        ----------
        transition: tensor of shape [transition_dim]
            The transition to add to the replay buffer
        """

        if self.nb < self.capacity:
            self.buffer[self.nb, :] = transition
            self.nb += 1
        else:
            self.buffer[self.older, :] = transition
            self.older = (self.older + 1) % self.capacity

    def sample(self, n):
        """
        Sample a given number of transitions from the replay buffer.

        Parameters
        ----------
        n: integer
            The number of transitions to sample from the replay buffer

        Return
        ------
        sampled: tensor of shape [n, transition_dim]
            The sampled transitions
        """

        indexes = np.random.randint(low=0, high=self.nb, size=n)

        return self.buffer[indexes, :]
